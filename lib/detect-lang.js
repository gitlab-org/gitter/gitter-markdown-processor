'use strict';

const cld = require('cld');
const Promise = require('bluebird');

module.exports = exports = function detectLang(text) {
  return Promise.fromCallback(callback => {
    cld.detect(text, callback);
  })
    .then(result => {
      if (!result || !result.languages || !Array.isArray(result.languages)) return;

      // Sometimes there are undefined values in the array
      // Seems to be when the result is unreliable
      const langs = result.languages.filter(f => Boolean(f));

      const primary = langs.shift();

      if (!primary) return;

      return primary.code;
    })
    .catch(() => {
      // Ignore errors
    });
};
