'use strict';

const workerFarm = require('worker-farm');
const htmlencode = require('htmlencode');
const { version } = require('../package.json');

function Processor({ camoUrl, camoSecret } = {}) {
  this.options = {
    camoUrl,
    camoSecret,
  };

  this.farm = workerFarm(
    {
      maxConcurrentWorkers: 1,
      maxConcurrentCallsPerWorker: 1,
      maxCallTime: 3000,
    },
    require.resolve('./process-chat-async'),
  );
}

Processor.prototype.process = function(text, callback) {
  this.farm(
    text,
    { camoUrl: this.options.camoUrl, camoSecret: this.options.camoSecret },
    (err, result) => {
      if (err && err.type === 'TimeoutError') {
        result = {
          text,
          html: htmlencode.htmlEncode(text).replace(/\n|&#10;/g, '<br>'),
          urls: [],
          mentions: [],
          issues: [],
          markdownProcessingFailed: true,
        };
      }

      callback(err, result);
    },
  );
};

Processor.prototype.shutdown = function(callback) {
  workerFarm.end(this.farm, callback);
};

Processor.version = version;

module.exports = Processor;
